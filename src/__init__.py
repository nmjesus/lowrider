# -*- coding: utf-8 -*-

from common.utils import log
from common.constants import APP_NAME, MESSAGE_TYPE
from common.sqlite import SQLite
import common.config as config
import common.events as event

""" TEST ONLY """
#import core.gstreamer

def bootstrap():
    config.set_paths()
    sql = SQLite()
    event.publish('application_starting')
    log("starting {0}...".format(APP_NAME), MESSAGE_TYPE["SUCCESS"])
    sql.close()


if __name__ == "__main__":
    bootstrap()
