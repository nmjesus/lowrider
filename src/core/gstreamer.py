# -*- coding: utf-8 -*-

import pygst
pygst.require('0.10')
import gst
import time

class GStreamer(object):
    inst = None
    current = None

    def __new__(self, *args, **kwargs):
        if self.inst is None:
            self.inst = super(GStreamer, self).__new__(self, args, kwargs)
        return self.inst

    def __init__(self):
        pass;
