# -*- coding: utf-8 -*-

from termcolor import colored

def is_function(fn):
    return isinstance(fn, types.FunctionType)

def log(msg="", type="success"):
    if type == 1:
        print(colored(msg, "green"))
    elif type == 0:
        print(colored("[ERROR] " + msg, "red"))
    elif type == 2:
        print(colored("[WARNING] " + msg, "red"))
    elif type == 3:
        print(colored(msg, "blue"))
    else:
        print(msg)
