# -*- coding: utf-8 -*-

from os import environ, makedirs
from os.path import join, isdir, expanduser
from common.utils import log
from common.constants import PATH, MESSAGE_TYPE

def set_paths():
    def create(key, name):
        try:
            if environ.get('XDG_CONFIG_HOME'):
                PATH[key] = join(environ.get('XDG_CONFIG_HOME'), name)
            else:
                PATH[key] = join(expanduser('~'), '.config', name)
            if not isdir(PATH[key]):
                try:
                    makedirs(PATH[key])
                except:
                    msg = "Erro creating path {0}".format(PATH[key])
                    log(msg, MESSAGE_TYPE["ERROR"])
                    raise
        except:
            msg = "Cannot load {0} path".format(PATH[key])
            log(msg, MESSAGE_TYPE["ERROR"])
            raise

    for k in PATH:
        create(k, PATH[k])
