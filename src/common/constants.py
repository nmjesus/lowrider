# -*- coding: utf-8 -*-
from os import path

APP_NAME    = 'lowrider'
DB_NAME     = APP_NAME + '.db'

MESSAGE_TYPE = {
    'ERROR':    0,
    'SUCCESS':  1,
    'WARNING':  2,
    'INFO':     3
}

PATH = {
    'conf': APP_NAME,
    'db': path.join(APP_NAME, 'db')
}

