# -*- coding: utf-8 -*-

import types
from common.utils import log, is_function

events = []

def subscribe(name, handler):
    events.append({'name': name, 'handler': handler});

def publish(name, msg=None):
    for o in events:
        if o['name'] == name and is_function(o['handler']):
            try:
                if not msg:
                    o["handler"]()
                else:
                    o["handler"](msg)
            except:
                msg = 'error calling {0} function'.format(o['handler'])
                log(msg, MESSAGE_TYPE['ERROR'])
