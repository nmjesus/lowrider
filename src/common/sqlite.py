# -*- coding: utf-8 -*-

import sqlite3
from common.constants import PATH, DB_NAME, MESSAGE_TYPE
from os.path import join, exists
from common.utils import log
import common.events as event

class SQLite(object):
    conn = None

    def __init__(self):
        self.connect()

    def connect(self):
        sqlfile = join(PATH['db'], DB_NAME)

        if not exists(sqlfile):
            create_schema = True
        else:
            create_schema = False

        self.conn = sqlite3.connect(sqlfile);
        self.cursor = self.conn.cursor()
        self.conn.text_factory = str;

        if create_schema:
            self.create_schema()

    def create_schema(self):
        log('Creating schema...', MESSAGE_TYPE['INFO'])
        self.cursor.execute(''
            'CREATE TABLE song ('
                'artist text,'
                'title text,'
                'album text,'
                'genre text,'
                'year integer,'
                'track integer,'
                'length integer,'
                'filename text'
        ')')
        self.conn.commit()

    def close(self):
        self.conn.close()
