APP_NAME=lowrider
CONFIG_PATH=~/.config
DEPS_FILE=./requirements.txt
PIP=`which pip`

all: run clean uninstall

run:
	@python src/__init__.py

clean:
	@find . -name *.pyc | xargs -I {} rm {}

uninstall:
	@rm -rf $(CONFIG_PATH)/$(APP_NAME)

install_deps:
	@pip install `cat $(DEPS_FILE)`

uninstall_deps:
	@pip uninstall `cat $(DEPS_FILE)`
